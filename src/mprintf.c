#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "main.h"
#include "uart.h"

extern UART_HandleTypeDef UartHandle;

void vprint(const char *fmt, va_list argp)
{
    char string[256];
    int cnt;
    cnt = vsnprintf(string,sizeof(string),fmt,argp);
    if(0 < cnt) // build string
    {
		if(HAL_UART_Transmit(&UartHandle, (uint8_t*)string, cnt , 5000)!= HAL_OK)
		{
			Error_Handler();
		}
    }
}

void mprintf(const char *fmt, ...) // custom printf() function
{
    va_list argp;
    va_start(argp, fmt);
    vprint(fmt, argp);
    va_end(argp);
}

void mputc(uint8_t c){
	if(HAL_UART_Transmit(&UartHandle, (uint8_t*)&c, 1 , 5000)!= HAL_OK)
	{
		Error_Handler();
	}
}
void mputs(uint8_t* buf , int length){
    if(0 < length) // build string
    {
		if(HAL_UART_Transmit(&UartHandle, (uint8_t*)buf, length , 5000)!= HAL_OK)
		{
			Error_Handler();
		}
    }
}
