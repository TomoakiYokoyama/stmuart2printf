################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/main.c \
../src/mprintf.c \
../src/stm32f0xx_hal_msp.c \
../src/stm32f0xx_it.c \
../src/syscalls.c \
../src/system_stm32f0xx.c \
../src/uart.c 

OBJS += \
./src/main.o \
./src/mprintf.o \
./src/stm32f0xx_hal_msp.o \
./src/stm32f0xx_it.o \
./src/syscalls.o \
./src/system_stm32f0xx.o \
./src/uart.o 

C_DEPS += \
./src/main.d \
./src/mprintf.d \
./src/stm32f0xx_hal_msp.d \
./src/stm32f0xx_it.d \
./src/syscalls.d \
./src/system_stm32f0xx.d \
./src/uart.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft -DSTM32F0 -DSTM32F091RCTx -DNUCLEO_F091RC -DSTM32 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F091xC -I"C:/Users/Tomoaki Yokoyama/STM32Workspace/nucleo-f091rc_hal_lib" -I"C:/Users/Tomoaki Yokoyama/STM32Workspace/UartTest/inc" -I"C:/Users/Tomoaki Yokoyama/STM32Workspace/nucleo-f091rc_hal_lib/CMSIS/core" -I"C:/Users/Tomoaki Yokoyama/STM32Workspace/nucleo-f091rc_hal_lib/CMSIS/device" -I"C:/Users/Tomoaki Yokoyama/STM32Workspace/nucleo-f091rc_hal_lib/HAL_Driver/Inc/Legacy" -I"C:/Users/Tomoaki Yokoyama/STM32Workspace/nucleo-f091rc_hal_lib/HAL_Driver/Inc" -I"C:/Users/Tomoaki Yokoyama/STM32Workspace/nucleo-f091rc_hal_lib/Utilities/STM32F0xx-Nucleo" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


